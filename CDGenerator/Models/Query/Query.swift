//
//  Query.swift
//  CDGenerator
//
//  Created by Ahd on 4/22/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class Query: NSObject {
    var entity: Entity?
    var conditions: [QueryCondition]?
    var sortBy: [QuerySort]?
}

class QuerySort: NSObject {
    var attribute: Attribute?
    var ascending: Bool?
}
