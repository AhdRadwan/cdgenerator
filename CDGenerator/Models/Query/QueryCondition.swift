//
//  QueryCondition.swift
//  CDGenerator
//
//  Created by Ahd on 4/22/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class QueryCondition: NSObject {
    
    var entity: Entity?
    var attribute: Attribute?
    var conditionCode: String??
    var compareValue: String?
    var inverted: Bool?
    var caseInsensitive: Bool?
}
