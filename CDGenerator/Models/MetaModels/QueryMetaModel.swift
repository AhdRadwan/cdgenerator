//
//  QueryMetaModel.swift
//  CDGenerator
//
//  Created by Ahd on 4/23/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class QueryMetaModel: NSObject {
    
    var queryParameters: [String]?
    var predicateFormatString: String?
    var predicateArgs: [String]?
    var sortDescriptorsCode: String?;
    var entityName: String?;
    var modelName: String?;

}
