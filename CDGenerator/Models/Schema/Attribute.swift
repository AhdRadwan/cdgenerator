//
//  Attribute.swift
//  CDGenerator
//
//  Created by Ahd on 4/12/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

enum AttributeType: String {
    case date = "Date"
    case string = "String"
    case boolean = "Boolean"
    case double = "Double"
    case float = "Float"
    case decimal = "Decimal"
    case int16 = "Integer 16"
    case int32 = "Integer 32"
    case int64 = "Integer 64"
    case uuid = "UUID"
    case uri = "URI"
    case any

}

class Attribute: NSObject {
    var name: String?;
    var optional: Bool?;
    var attributeType: AttributeType?;
    var defaultValueString: String?;
    var usesScalarValueType: Bool?;
    
    static func parse(attributes: [String : Any]) -> Attribute{
        let object = Attribute();
        object.name = attributes["name"] as? String
        object.optional = attributes["optional"] as? Bool
        object.attributeType = AttributeType(rawValue: attributes["attributeType"] as! String)
        object.defaultValueString = attributes["defaultValueString"] as? String
        object.usesScalarValueType = attributes["usesScalarValueType"] as? Bool
        return object;
    }

}
