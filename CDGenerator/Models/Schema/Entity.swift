//
//  Entity.swift
//  CDGenerator
//
//  Created by Ahd on 4/12/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class Entity: NSObject {
    var name: String? = "";
    var modelName: String? = "";
    var attributes: [Attribute]?;
    var parentEntity: String?;

    var relationships: [Relationship]?
    
    static func parse(attributes: [String : Any]) -> Entity{
        let object = Entity();
        object.name = attributes["name"] as? String
        object.modelName = object.name! + "Model"
        object.parentEntity = attributes["parentEntity"] as? String

        return object;
    }
    
}
