//
//  Relationship.swift
//  CDGenerator
//
//  Created by Ahd on 4/24/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class Relationship: NSObject {
    var name: String?
    var optional: Bool?
    var maxCount: Int?;
    var destinationEntity: String?;
    var deletionRule: String?
    var inverseName: String?
    var inverseEntity: String?
    var toMany: Bool?

    static func parse(attributes: [String : Any]) -> Relationship{
        let object = Relationship();
        object.name = attributes["name"] as? String
        object.optional = (attributes["optional"] as? String) == "YES" ? true : false;
        object.maxCount = Int((attributes["maxCount"] as? String) ?? "1")
        object.destinationEntity = attributes["destinationEntity"] as? String
        object.deletionRule = attributes["deletionRule"] as? String
        object.inverseName = attributes["inverseName"] as? String
        object.inverseEntity = attributes["inverseEntity"] as? String
        object.toMany = (attributes["toMany"] as? String) == "YES" ? true : false;

        return object;
    }
}
//<relationship name="messages" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="Messages" inverseName="chat" inverseEntity="Messages"/>
//<relationship name="chat" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Chats" inverseName="messages" inverseEntity="Chats"/>
