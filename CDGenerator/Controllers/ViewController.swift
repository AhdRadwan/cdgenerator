//
//  ViewController.swift
//  CDGenerator
//
//  Created by Ahd on 3/22/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var fileTextField: NSTextField!
    @IBOutlet weak var browsButton: NSButton!
    @IBOutlet weak var addTargetFolder: NSButton!
    
    @IBOutlet weak var targetTextField: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


    @IBAction func didClickBrowsButton(_ sender: Any) {
        let dialog = NSOpenPanel();

        dialog.title                   = "Choose Your data schema file file";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = true;
        dialog.canChooseDirectories    = false;
        dialog.canCreateDirectories    = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = ["xcdatamodeld"];

        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            let result = dialog.url // Pathname of the file
            
            if (result != nil) {
                let path = result!.path
                fileTextField.stringValue = path
                self.readFileContent(fileURL: URL(fileURLWithPath: path));
            }
        } else {
            // User clicked on "Cancel"
            return
        }
        
    }
    
    @IBAction func didClickAddTargetFolder(_ sender: Any) {
    let dialog = NSOpenPanel();

    dialog.title                   = "Choose your files target directory";
    dialog.showsResizeIndicator    = true;
    dialog.showsHiddenFiles        = true;
    dialog.canChooseDirectories    = true;
    dialog.canChooseFiles = false;
    dialog.canCreateDirectories    = true;
    dialog.allowsMultipleSelection = false;

    if (dialog.runModal() == NSApplication.ModalResponse.OK) {
        let result = dialog.url // Pathname of the file
        
        if (result != nil) {
            let path = result!.path
            targetTextField.stringValue = path
            FilesManager.shared.userSelectedPath = path;
        }
    } else {
        // User clicked on "Cancel"
        return
    }

    }
    
    @IBAction func didClickGoButton(_ sender: Any) {
        FilesManager.shared.userSelectedPath = targetTextField.stringValue;
        let schema = self.fileTextField.stringValue;
        SchemaManager.shared.fetchScemaData(schema: schema);
//        FilesManager.shared.sveText();

        }
   
    
    //MARK : utils
    func readFileContent(fileURL: URL) {
        
        //reading
        do {
            let text2 = try String(contentsOf: fileURL, encoding: .utf8)
            print("text is :" + text2);
        }
        catch {/* error handling here */
            print(error);
            
        }
    }
    
    func readXML(string: String) {
        
    }

}

