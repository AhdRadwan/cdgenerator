//
//  BuildQueryViewController.swift
//  CDGenerator
//
//  Created by Ahd on 4/21/20.
//  Copyright © 2020 ahd. All rights reserved.
//

enum ConditionEnum: Int {
    case graterThan = 1
    case lessThan
    case equals
    case graterThanOrEquals
    case lessThanOrEquals
    case contains
    case beginWith
    case endsWith
    case regex
    case inArray
}

import Cocoa

class BuildQueryViewController: NSViewController {

    @IBOutlet weak var entityPopupButton: NSPopUpButton?
    @IBOutlet weak var attributePopupButton: NSPopUpButton?

    @IBOutlet weak var soortByPopupButton: NSPopUpButton?
    
    @IBOutlet weak var conditionCodeTextField: NSTextField?
    
    @IBOutlet weak var sortTypePopupButton: NSPopUpButton!
    @IBOutlet weak var conditionValueTextField: NSTextField?
    
    @IBOutlet weak var invertSwitch: NSSwitch?
    
    @IBOutlet weak var caseInsensitiveSwitch: NSSwitch?

    @IBOutlet var queryTextView: NSTextView!
    
    var selectedRadio: NSButton?
    var conditions = [QueryCondition]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        self.initView();
        
    }
    func initView() {
        self.configEntityPopupButton();
        self.configAttributePopupButton();
        
        self.conditionCodeTextField?.delegate = self;
        self.sortTypePopupButton.isHidden = true;
        
    }
    override func viewWillAppear() {
        self.configEntityPopupButton();
        self.configAttributePopupButton();

    }
    func configEntityPopupButton() {
        self.entityPopupButton?.removeAllItems()
        for entity in CodeGenerator.shared.entities ?? [] {
            self.entityPopupButton?.addItem(withTitle: entity.name!);
            
        }
        self.entityPopupButton?.selectItem(at: 0)
    }
    func configAttributePopupButton() {
        
        self.attributePopupButton?.removeAllItems()
        self.soortByPopupButton?.removeAllItems()
        self.soortByPopupButton?.addItem(withTitle: "Select attribute");
        let selectedEntity = CodeGenerator.shared.entities?[self.entityPopupButton!.indexOfSelectedItem];
        
        for attribute in selectedEntity?.attributes ?? [] {
            self.attributePopupButton?.addItem(withTitle: attribute.name!);
            self.soortByPopupButton?.addItem(withTitle: attribute.name!);
        }
        self.attributePopupButton?.selectItem(at: 0);

    }
    
    //MARK: - actions

    @IBAction func radioButtonValueChanged(_ sender: Any) {
        self.selectedRadio = (sender as! NSButton);
        
        let condition = ConditionEnum(rawValue:(sender as! NSView).tag);
        
        var conditionValue = "";
        switch condition {
        case .graterThan:
            conditionValue = ">"
        case .lessThan:
            conditionValue = "<"
        case .equals:
            conditionValue = "=="
        case .graterThanOrEquals:
            conditionValue = ">="
        case .lessThanOrEquals:
            conditionValue = "<="
        case .contains:
            conditionValue = "CONTAINS"
        case .beginWith:
            conditionValue = "BEGINSWITH"
        case .endsWith:
            conditionValue = "ENDSWith"
        case .regex:
            conditionValue = "MATCHES"
            
            
        case .inArray:
            conditionValue = "in"
            
        default:
            conditionValue = ""
        }
        self.selectedRadio = (sender as! NSButton);
        self.conditionCodeTextField!.stringValue = conditionValue
        
    }
    
    //MARK: - Utils

    func addCondition() {
        let condition = QueryCondition();

        let selectedEntity = CodeGenerator.shared.entities?[self.entityPopupButton!.indexOfSelectedItem];
        let selectedAttribute = selectedEntity?.attributes?[self.attributePopupButton!.indexOfSelectedItem];
        
        if (selectedEntity == nil || selectedAttribute == nil) {
            self.showAlert(title: "Missing field", message: "Select entity and attribute");
            return;
        }
        
        if (self.conditionCodeTextField?.stringValue == nil || (self.conditionCodeTextField?.stringValue.isEmpty)!) {
            self.showAlert(title: "Missing Condition", message: "Please Select a condition");
            return;
        }

        
        condition.entity = selectedEntity;
        condition.attribute = selectedAttribute;
        condition.conditionCode = self.conditionCodeTextField?.stringValue;
        condition.compareValue = self.conditionValueTextField?.stringValue;
        condition.caseInsensitive = self.caseInsensitiveSwitch?.state == .on;
        condition.inverted = self.invertSwitch?.state == .on;
        
        self.conditions.append(condition);
        self.generateQuery();
        self.resetCurrentAttribute();
    }
    
    func generateQuery() {
        let query = Query();
        query.conditions = self.conditions;
        let selectedEntity = CodeGenerator.shared.entities?[self.entityPopupButton!.indexOfSelectedItem];

        query.entity = selectedEntity;
        
        if self.soortByPopupButton!.indexOfSelectedItem > 0 {
                    let sortSelectedAttribute = selectedEntity?.attributes?[self.soortByPopupButton!.indexOfSelectedItem - 1];
            let ascending = self.sortTypePopupButton.indexOfSelectedItem == 0;
            let sortBy = QuerySort.init();
            sortBy.attribute = sortSelectedAttribute;
            sortBy.ascending = ascending;
            query.sortBy = [sortBy];

        }
        let queryMetaModel = MetaModelsManager.queryMetaModel(from: query);
        let code = CodeGenerator.shared.codeFor(queryMetaModel: queryMetaModel);

        self.queryTextView.string = code;
    }
    
    func showAlert(title: String, message: String) {
        let alert = NSAlert()
        alert.messageText = title;
        alert.informativeText = message
        alert.addButton(withTitle: "OK")
        alert.runModal()
    }
    
    func reset() {
        self.configEntityPopupButton();
        self.configAttributePopupButton();
        self.resetCurrentAttribute();
        self.queryTextView.string = "";
        self.conditions.removeAll();
        self.sortTypePopupButton.isHidden = true
    }
    func resetCurrentAttribute() {
        self.conditionValueTextField?.stringValue = "";
        self.conditionCodeTextField?.stringValue = "";
        self.caseInsensitiveSwitch?.state = .off
        self.invertSwitch?.state = .off
        self.resetSelectedRadioButton();
    }
    func resetSelectedRadioButton() {
        self.selectedRadio?.state = .off;
        self.selectedRadio = nil;

    }
    
    @IBAction func didClickAddAction(_ sender: Any) {
        self.addCondition();
    }
    
    @IBAction func didClickResetButton(_ sender: Any) {
        self.reset();
    }
    @IBAction func didSelectEnttiyItem(_ sender: Any) {
        self.configAttributePopupButton();
    }
    @IBAction func didSelectAttributeItem(_ sender: Any) {
        
    }
    
    @IBAction func didSelectSortByItem(_ sender: Any) {
    }
    
    @IBAction func didClickAddCondition(_ sender: Any) {
        //        todo:generate code
        self.addCondition();
    }
    
    @IBAction func didSelectSortItem(_ sender: Any) {
        self.sortTypePopupButton.isHidden = false;
    }
    @IBAction func didClickGenerateQuerryButton(_ sender: Any) {
        self.generateQuery();
    }
}

extension BuildQueryViewController :NSTextFieldDelegate {
    func controlTextDidChange(_ obj: Notification) {
        if let textField = obj.object as? NSTextField {
            if textField == self.conditionCodeTextField {
                self.resetSelectedRadioButton();
            }
        }
    }
}
