//
//  MetaModelsManager.swift
//  CDGenerator
//
//  Created by Ahd on 4/24/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class MetaModelsManager: NSObject {

    static func queryMetaModel(from query: Query) -> QueryMetaModel{
        
        let metaModel = QueryMetaModel();
        
        metaModel.queryParameters = [String]();
        metaModel.predicateFormatString = ""
        metaModel.predicateArgs = [String]();
        metaModel.sortDescriptorsCode = ""
        metaModel.entityName = query.entity?.name;
        metaModel.modelName = query.entity?.modelName;
        
        for condition in query.conditions! {
            var arg: String?;
            var argsParameter: String?;
            var compareWith: String;
            let invertKey = condition.inverted! ? "NOT " : "";
            let compareStatement = condition.conditionCode!; //todo trim
            let attribute = condition.attribute?.name;
            let caseInsensitiveKey = condition.caseInsensitive! ? "[c]" : "";
            let format = CodeUtils.formatString(attributeType: condition.attribute!.attributeType);
            
            
            if condition.compareValue!.isEmpty {
                let format = CodeUtils.formatString(attributeType: condition.attribute!.attributeType);
                compareWith = format;

                if compareStatement == "in"  {
                    //Compare array
                    arg  =  attribute! + "List"
                    argsParameter  =  CodeUtils.queryParameterFor(attribute:condition.attribute!, isArray: false)
                    
                } else {
                    arg = attribute!;
                    argsParameter  =  CodeUtils.queryParameterFor(attribute:condition.attribute!, isArray: false)
                }
                metaModel.queryParameters!.append(argsParameter!);
            } else {
                let format = CodeUtils.formatString(attributeType: .string);
                compareWith = format
                arg = "\"" + condition.compareValue! + "\"";
//                compareWith = condition.compareValue!;  //todo trim
            }
            metaModel.predicateArgs!.append(arg!);

            
            let conditionStatement = invertKey + attribute! + " " + compareStatement! + caseInsensitiveKey + " " + compareWith + " ";
            
            metaModel.predicateFormatString! += conditionStatement + " AND "
            print(conditionStatement);
        }
        metaModel.sortDescriptorsCode = ""
        if query.sortBy != nil && query.sortBy!.count > 0 {
            let sortKey = query.sortBy?.first?.attribute!.name;
            let ascendingCode = (query.sortBy?.first!.ascending)! ? "true" : "false";
            let sortDescriptorsLineCode = String(format: kSortDescriptorsLineCode, sortKey!, ascendingCode);
            metaModel.sortDescriptorsCode = sortDescriptorsLineCode;
        }
        
        metaModel.predicateFormatString = String(metaModel.predicateFormatString!.dropLast(5));

        return metaModel;
    }
}
