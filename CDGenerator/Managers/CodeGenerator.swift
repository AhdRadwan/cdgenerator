//
//  CodeGenerator.swift
//  CDGenerator
//
//  Created by Ahd on 4/14/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class CodeGenerator: NSObject {

    static let shared = CodeGenerator();
    var entities: [Entity]?
    var appModuleName: String? = "Sample"//todo: change modile

    private override init() {
        super.init();
    }
    
    func generateModelsFiles() {
        for entity in self.entities! {
            self.generateModelFor(entity: entity);
        }
    }
    
    func generateModelFor(entity: Entity) {
        var modelClassCode = "";
        
        modelClassCode +=  "import UIKit\n" // import statment
        modelClassCode +=  "import CoreData\n" // import statment
        modelClassCode += "\n"
        modelClassCode +=  CodeUtils.stringForClassDeclaration(entity: entity)
        modelClassCode +=  " {\n\n"; //Start of model class
        
        for attribute in entity.attributes! { //Properties
            modelClassCode += "\t" + CodeUtils.propertyDeclarationCodeFor(attribute: attribute ) + "\n"; //Property declaration
        }
        
        for relationship in entity.relationships! { //Properties
            modelClassCode += "\t" + CodeUtils.relationshipDeclarationCodeFor(relationship: relationship ) + "\n"; //Relationship declaration
        }
        
       modelClassCode +=  kInitMethodCode
        modelClassCode +=  "\n"

        modelClassCode += self.generateParserMethodFor(entity: entity);
        modelClassCode += "\n}" //End of model class
    
        FilesManager.shared.saveFile(code: modelClassCode, fileName: entity.modelName!)
    }
    
    func generateParserMethodFor(entity: Entity) -> String {
        var parserMethodCode = ""
        parserMethodCode += "\n\t"
        parserMethodCode += "init(managedObject: NSManagedObject) {" //Init method declaration
        parserMethodCode += "\n\n"
        
        for attribute in entity.attributes! { //fetch properties
            parserMethodCode += "\t\t" + CodeUtils.propertyFetchLineCode(attribute: attribute ) + "\n"; //Property fetch line code
        }
        parserMethodCode += "\n\t}\n" //End of method
        
        parserMethodCode += self.generateSaveMethodFor(entity: entity);
        parserMethodCode += self.generateDeleteMethodFor(entity: entity);
        parserMethodCode += self.gernarateModelIdentityMethodFor(entity: entity)
        return parserMethodCode;
    }
    
    func generateSaveMethodFor(entity: Entity) -> String {
        var saveMethodCode = ""
        saveMethodCode += "\n\t"
        saveMethodCode += "func save() {" //Save method declaration
        saveMethodCode += "\n\n"
      
        saveMethodCode +=
            "\t\t" + "if !Thread.current.isMainThread {" + "\n" +
            "\t\t\t" +  "DispatchQueue.main.async {" + "\n" +
            "\t\t\t\t" +  "self.save()" + "\n" +
            "\t\t\t" + "}" + "\n" +
            "\t\t\t" + "return" + "\n" +
            "\t\t" + "}" + "\n"

        let saveFormat = "CDQueriesManager.shared.save(%@: self)"
        let saveLine = String.init(format: saveFormat, entity.name!.camelized);
        saveMethodCode += "\t\t" + saveLine + "\n"

        saveMethodCode += "\t}\n" //End of method
        return saveMethodCode;
        
    }
    func generateDeleteMethodFor(entity: Entity) -> String {
        var deleteMethodCode = ""
        deleteMethodCode += "\n\t"
        deleteMethodCode += "func delete() {" //Delete method declaration
        deleteMethodCode += "\n\n"
      
        deleteMethodCode +=
            "\t\t" + "if !Thread.current.isMainThread {" + "\n" +
            "\t\t\t" +  "DispatchQueue.main.async {" + "\n" +
            "\t\t\t\t" +  "self.delete()" + "\n" +
            "\t\t\t" + "}" + "\n" +
            "\t\t\t" + "return" + "\n" +
            "\t\t" + "}" + "\n"

        let deleteFormat = "CDQueriesManager.shared.delete(%@: self)"
        let deleteLine = String.init(format: deleteFormat, entity.name!.camelized);
        deleteMethodCode += "\t\t" + deleteLine + "\n"

        deleteMethodCode += "\t}\n" //End of method
        return deleteMethodCode;
    }
    
    func gernarateModelIdentityMethodFor(entity: Entity) -> String{
        var modelIdentityCode = kModelIdentityCode;
        modelIdentityCode = modelIdentityCode.replacingOccurrences(of: kModelNameKey, with: entity.modelName!);
        return modelIdentityCode;
    }

    //MARK:- Managers

    func genrateManagersFiles(){
        self.gernarateCoreDataManager();
        self.gernarateCDQueriesManager()
    }
    func gernarateCoreDataManager() {
        var coreDataManagerCode = FilesManager.shared.coreDataManagerFile();
        coreDataManagerCode = coreDataManagerCode.replacingOccurrences(of: kModuleKey, with: appModuleName!);
        FilesManager.shared.saveFile(code: coreDataManagerCode, fileName: "CoreDataManager");
    }
    
    //MARK:- CDQueriesManager
    func gernarateCDQueriesManager() {
        var queriesManagerCode = FilesManager.shared.CDQueriesManagerFile();
        
        let content = self.gernarateCDQueriesManagerContent();
        queriesManagerCode = queriesManagerCode.replacingOccurrences(of: kContentCodeKey, with: content);
        FilesManager.shared.saveFile(code: queriesManagerCode, fileName: "CDQueriesManager");
    }
    func gernarateCDQueriesManagerContent() -> String{
        var content = "";
        for entity in self.entities! {
            content += "\n//MARK:- " + entity.name! + " APIs"
            content += "\n" + self.gernarateStoreMethodCode(entity: entity) + "\n";
            content += "\n" + self.gernarateQueryEntityListMethodCode(entity: entity) + "\n";
            content += "\n" + self.gernarateQueryEntityWitthIdCode(entity: entity) + "\n";
            content += "\n" + self.gernarateDeleteMethodCode(entity: entity) + "\n";

        }
        return content;
    }
    
    func gernarateStoreMethodCode(entity: Entity) -> String{
        var storeMethodCode = kStoreEntityMethodCode;
        storeMethodCode = storeMethodCode.replacingOccurrences(of: kEntityNameKey, with: entity.name!);
        storeMethodCode = storeMethodCode.replacingOccurrences(of: kModelNameKey, with: entity.modelName!);
        storeMethodCode = storeMethodCode.replacingOccurrences(of: kEntityNameCamelCaseKey, with: entity.name!.camelized);
        
        var storeEntityValueCode = "";
        for attribute in entity.attributes! { //Properties
            storeEntityValueCode += "\t\t" + CodeUtils.propertySavingLineCode(attribute: attribute, entity: entity ) + "\n"; //Property saving
        }
        storeMethodCode = storeMethodCode.replacingOccurrences(of: kStoreEntityValueKey, with: storeEntityValueCode);
        
        return storeMethodCode;
    }
    
    
    func gernarateQueryEntityListMethodCode(entity: Entity) -> String{
        var queryEntityListMethodCode = kQueryEntityListCode;
        queryEntityListMethodCode = queryEntityListMethodCode.replacingOccurrences(of: kEntityNameKey, with: entity.name!);
        queryEntityListMethodCode = queryEntityListMethodCode.replacingOccurrences(of: kModelNameKey, with: entity.modelName!);
                
        return queryEntityListMethodCode;
    }
    func gernarateDeleteMethodCode(entity: Entity) -> String{
        var deleteMethodCode = kDeleteEntityMethodCode;
        deleteMethodCode = deleteMethodCode.replacingOccurrences(of: kEntityNameKey, with: entity.name!);
        deleteMethodCode = deleteMethodCode.replacingOccurrences(of: kModelNameKey, with: entity.modelName!);
        deleteMethodCode = deleteMethodCode.replacingOccurrences(of: kEntityNameCamelCaseKey, with: entity.name!.camelized);
                
        return deleteMethodCode;
    }
    func gernarateQueryEntityWitthIdCode(entity: Entity) -> String{
        var queryEntityWitthIdCode = kQueryEntityWithIdCode;
        queryEntityWitthIdCode = queryEntityWitthIdCode.replacingOccurrences(of: kEntityNameKey, with: entity.name!);
        queryEntityWitthIdCode = queryEntityWitthIdCode.replacingOccurrences(of: kModelNameKey, with: entity.modelName!);

        return queryEntityWitthIdCode;
    }
    
    //MARK: - API and queries
    func codeFor(queryMetaModel: QueryMetaModel) -> String {
        
        
        var fetchQueryCode = kCustomQueryCode;

        var predicateArgumentString = "";
        var argsParameterString = "";

        for arg in queryMetaModel.predicateArgs! {
            predicateArgumentString += ", "
            predicateArgumentString += arg;
        }

        for argParameter in queryMetaModel.queryParameters! {
            argsParameterString += argParameter;
            argsParameterString += ", "
        }
        argsParameterString = String(argsParameterString.dropLast(2))
        
        var predicateLineCode = kPredicateLineCode;
        predicateLineCode = predicateLineCode.replacingOccurrences(of: kQueryPredicateFormatKey, with: queryMetaModel.predicateFormatString!);
        predicateLineCode = predicateLineCode.replacingOccurrences(of: kQueryPredicateArgsKey, with: predicateArgumentString);
        
        
        var fetchQueryLineCode = queryMetaModel.sortDescriptorsCode != nil && !queryMetaModel.sortDescriptorsCode!.isEmpty
            ? kFetchQueryLineCodeWithWithSort
            : kFetchQueryLineCodeWithWithoutSort;
        
        fetchQueryLineCode = fetchQueryLineCode.replacingOccurrences(of: kEntityNameKey, with: queryMetaModel.entityName!);
        
        fetchQueryCode = fetchQueryCode.replacingOccurrences(of: kEntityNameKey, with: queryMetaModel.entityName!);
        fetchQueryCode = fetchQueryCode.replacingOccurrences(of: kModelNameKey, with: queryMetaModel.modelName!);

        fetchQueryCode = fetchQueryCode.replacingOccurrences(of: kQueryParameters, with: argsParameterString);
        fetchQueryCode = fetchQueryCode.replacingOccurrences(of: kPredicateLineKey, with: predicateLineCode);
        fetchQueryCode = fetchQueryCode.replacingOccurrences(of: kFetchQueryLineKey, with: fetchQueryLineCode);

        fetchQueryCode = fetchQueryCode.replacingOccurrences(of: kSortDescriptorsLineKey, with: queryMetaModel.sortDescriptorsCode!);

        
        return fetchQueryCode
    }

}
