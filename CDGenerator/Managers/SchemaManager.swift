//
//  SchemaManager.swift
//  CDGenerator
//
//  Created by Ahd on 4/13/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class SchemaManager: NSObject, XMLParserDelegate {
    
    static let shared = SchemaManager();
    
    var entities: [Entity]?
    var currentEntity: Entity?

    private override init() {
        super.init();
    }
    func fetchScemaData(schema: String) {
        self.entities = [Entity]();
        do {
            let element = try XMLElement(xmlString: schema);
            
            //converting into NSData
            let data: Data? = schema.data(using: .utf8)
            
            //initiate  NSXMLParser with this data
            let parser: XMLParser? = XMLParser(data: data ?? Data())
            
            //setting delegate
            parser?.delegate = self as XMLParserDelegate
            
            //call the method to parse
            var result: Bool? = parser?.parse()
            
            parser?.shouldResolveExternalEntities = true
            
        } catch {
            self.showFailerAlert();
        }
        
    }
    func showSuccessAlert() {
        let alert = NSAlert()
        alert.messageText = "Success";
        alert.informativeText = "Your data Schema featched successfully"
        //        alert.addButton(withTitle: "Data parsed Successfully")
        
        alert.addButton(withTitle: "OK")
        alert.runModal()
        
    }
    func showFailerAlert() {
        let alert = NSAlert()
        alert.messageText = "Failed";
        alert.informativeText = "Invalid data shema"
        //        alert.addButton(withTitle: "Data parsed Successfully")
        alert.addButton(withTitle: "OK")
        alert.runModal()
        
    }


    //MARK: XMLParserDelegate
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
       
        let currentElement = elementName
        
        print("CurrentElementl: [\(elementName)]")
        
        switch elementName {
        case "entity":
            let entity = Entity.parse(attributes: attributeDict);
            self.entities?.append(entity);
            self.currentEntity = entity;
            self.currentEntity!.attributes = [Attribute]();
            self.currentEntity!.relationships = [Relationship]();

        case "attribute" :
            let attribute = Attribute.parse(attributes: attributeDict);
            self.currentEntity?.attributes?.append(attribute);
            
        case "relationship" :
            let relationship = Relationship.parse(attributes: attributeDict);
            self.currentEntity?.relationships!.append(relationship);
            
        default: break
        }
        
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        if self.entities!.count > 0 {
            self.showSuccessAlert();

            CodeGenerator.shared.entities = entities;
            CodeGenerator.shared.generateModelsFiles();
            CodeGenerator.shared.genrateManagersFiles();

        }
    }
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        print("foundCharacters: [\(string)]")

    }
    
}
