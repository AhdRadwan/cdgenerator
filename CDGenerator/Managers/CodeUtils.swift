//
//  CodeUtils.swift
//  CDGenerator
//
//  Created by Ahd on 4/15/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class CodeUtils: NSObject {

    
    class func stringForClassDeclaration(entity: Entity) -> String{
        
        let propertyStringFormat = "class %@: %@"
        let superEntity =  entity.parentEntity ?? "NSObject"
        let classLineCode = String(format: propertyStringFormat, entity.modelName!, superEntity);
        
        return classLineCode;
    }

    class func propertyDeclarationCodeFor(attribute: Attribute) -> String{
        let type: String = self.stringFor(attributeType: attribute.attributeType);
        
        let propertyStringFormat = "var %@: %@?"
        let propertyLineCode = String(format: propertyStringFormat, attribute.name!, type);
        
        return propertyLineCode;
    }
    
    class func relationshipDeclarationCodeFor(relationship: Relationship) -> String{
        
        let entityModel = relationship.destinationEntity != nil ? (relationship.destinationEntity! + "Model") : "Any"
        let type: String = ((relationship.maxCount ?? 1) > 1 || (relationship.toMany ?? false)) ? "[\(entityModel)]" : entityModel;
        
        let propertyStringFormat = "var %@: %@?"
        let propertyLineCode = String(format: propertyStringFormat, relationship.name!, type);
        
        return propertyLineCode;
    }

    class func propertyFetchLineCode(attribute: Attribute) -> String{
        let type: String = self.stringFor(attributeType: attribute.attributeType);
        
        let propertyParserFormat = "self.%@ = (managedObject.value(forKey: \"%@\") as? %@)"
        let propertyParserLineCode = String(format: propertyParserFormat, attribute.name!, attribute.name!, type);
        
        return propertyParserLineCode;
    }
    class func propertySavingLineCode(attribute: Attribute, entity: Entity) -> String{

        let propertySavingFormat = "object.setValue(%@.%@, forKey: \"%@\")"
        let propertySavingLineCode = String(format: propertySavingFormat, entity.name!.camelized, attribute.name!, attribute.name!);
        return propertySavingLineCode;
    }

    
    class func stringFor(attributeType: AttributeType?) -> String{

        switch attributeType {
            
        case .date,.string,.float,.double,.decimal,.uuid,.uri:
            return attributeType!.rawValue;
        
        case .int16, .int32, .int64 :
            return "Int";
        case .boolean:
            return "Bool";

        default:
            return "Any"
        }
    }
    
    class func formatString(attributeType: AttributeType?) -> String{

        switch attributeType {
        case .float,.double,.decimal :
            return "%f";
        case .int16, .int32, .int64 :
            return "%ld";
        default:
            return "%@"
        }
    }
    
    class func queryParameterFor(attribute: Attribute, isArray:Bool) -> String{
        let type: String = self.stringFor(attributeType: attribute.attributeType);
        
        let parameterFormat = "%@: %@"
        let typeCode = isArray ? "[\(type)]" : type;
        let parameterCode = String(format: parameterFormat, attribute.name!, typeCode);
        
        return parameterCode;
    }

}
