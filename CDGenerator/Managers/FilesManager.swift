//
//  FilesManager.swift
//  CDGenerator
//
//  Created by Ahd on 4/14/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Cocoa

class FilesManager: NSObject {
    
    static let shared = FilesManager();
    var userSelectedPath: String?;
    
    private override init() {
        super.init();
    }
    
    func saveFile(code:String, fileName: String) {

        let filenamePath = getDownloadsDirectory().appendingPathComponent(fileName + ".swift")
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let dateString = formatter.string(from: Date());
        
        let str =
            "//  " + fileName + "\n" +
            "//  CDGenerator\n" +
            "//\n" +
            "//  Created by CDGenerator on " + dateString  + ".\n" +
            "//  Copyright © 2020 CDGenerator. All rights reserved.\n" +
            "//\n\n" + code
        print(str);

        do {
            try str.write(to: filenamePath, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
        }
    }
    
    func readFile(fileName: String, type: String) -> String? {
        let path = Bundle.main.url(forResource: fileName, withExtension: type)

        do {
            let string = try String(contentsOf: path!, encoding: String.Encoding.utf8)
            return string;
        }
        catch {
            print("Unable to read file")
            return nil;
        }
    }
    func coreDataManagerFile() -> String {
        return self.readFile(fileName: "CoreDataManager", type: "txt")!;
    }
    func CDQueriesManagerFile() -> String {
        return self.readFile(fileName: "CDQueriesManager", type: "txt")!;
    }

    
    func getDownloadsDirectory() -> URL {
        if (userSelectedPath == nil) {
            //No selected path
            //Then save files on Directory.
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            return paths[0]
            

        } else {
            // User selected target path
            //Then save files on Directory.
            
            let userSelectedPath = URL(fileURLWithPath: self.userSelectedPath!)
            return userSelectedPath;

        }
    }
    
    
}
