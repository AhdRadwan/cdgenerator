//
//  Constants.swift
//  CDGenerator
//
//  Created by Ahd on 4/17/20.
//  Copyright © 2020 ahd. All rights reserved.
//

import Foundation

//MARK: Keys
let kModuleKey = "@MODULE_NAME@"
let kEntityNameKey = "@ENTITY_NAME@"
let kModelNameKey = "@MODEL_NAME@"
let kEntityNameCamelCaseKey = "@ENTITY_NAME_CAMEL_CASE@"
let kStoreEntityValueKey = "@STORE_ENTITY_VALUES_CODE@"
let kContentCodeKey = "@CONTENT_CODE@"
let kPredicateLineKey = "@PREDICATE_LINE@"
let kSortDescriptorsLineKey = "@SORT_DESCRIPTORS_LINE@"
let kFetchQueryLineKey = "@FETCH_QUERY_LINE@"
let kQueryParameters = "@QUERY_PARAMETERS@"
let kQueryPredicateFormatKey = "@QUERY_PREDICATE_FORMAT@"
let kQueryPredicateArgsKey = "@QUERY_PREDICATE_ARGS@"

//MARK: Code constants


let kInitMethodCode = """

    override init() {
    
    }
"""

let kModelIdentityCode = """

    //MARK:- Model Identity
    var identityPredicate: NSPredicate {
        //TODO: Update identity predicate
    return NSPredicate.init(format: @MODEL_NAME@.identityKey + " = %@", self.identityValue)

    }
    static var identityKey: String {
        //TODO: If your model identity key is not 'id', you need to change update it from here
        return "id";
    }
    var identityValue: String {
        //TODO: If your model identity key is not 'id', you need to change update it from here
        return self.id!;
    }

"""

let kStoreEntityMethodCode = """

    func save(@ENTITY_NAME_CAMEL_CASE@: @MODEL_NAME@) {

        let context = CoreDataManager.shared.managedObjectContext
        let predicate = @ENTITY_NAME_CAMEL_CASE@.identityPredicate
        let fetchResults = CoreDataManager.shared.fetchRequestForEntity(entityName: "@ENTITY_NAME@", predicate: predicate)
        var object: NSManagedObject
        if(fetchResults !=  nil && fetchResults!.count > 0) {
            object = fetchResults?.first as! NSManagedObject
        } else {
            object = NSEntityDescription.insertNewObject(forEntityName: "@ENTITY_NAME@", into: context)
        }
        
@STORE_ENTITY_VALUES_CODE@
        CoreDataManager.shared.saveContext()
    }
"""

let kDeleteEntityMethodCode = """
    func delete(@ENTITY_NAME_CAMEL_CASE@: @MODEL_NAME@) {
        let context = CoreDataManager.shared.managedObjectContext;
        let predicate = @ENTITY_NAME_CAMEL_CASE@.identityPredicate;
        let fetchResults = CoreDataManager.shared.fetchRequestForEntity(entityName: "@ENTITY_NAME@", predicate: predicate);
        if(fetchResults !=  nil && fetchResults!.count > 0) {
            context.delete(fetchResults!.first as! NSManagedObject);
            CoreDataManager.shared.saveContext();
        }
    }
"""

let kQueryEntityListCode = """
    func query@ENTITY_NAME@List() -> [@MODEL_NAME@] {
        let fetchResults = CoreDataManager.shared.fetchRequestForEntity(entityName: "@ENTITY_NAME@", predicate: nil);
        var list = [@MODEL_NAME@]();
        for object in fetchResults! {
            let model = @MODEL_NAME@.init(managedObject: object as! NSManagedObject);
            list.append(model);
        }
        return list;
    }
"""

let kQueryEntityWithIdCode = """

    func query@ENTITY_NAME@(id: String) -> @MODEL_NAME@? {
        let predicate = NSPredicate.init(format: @MODEL_NAME@.identityKey + " = %@", id)

        let fetchResults = CoreDataManager.shared.fetchRequestForEntity(entityName: "@ENTITY_NAME@", predicate: predicate);
        if fetchResults != nil  && fetchResults!.first != nil {
            let model = @MODEL_NAME@.init(managedObject: fetchResults?.first as! NSManagedObject);
            return model;
        }
        return nil;
    }
"""

let kCustomQueryCode = """

func query@ENTITY_NAME@List(@QUERY_PARAMETERS@) -> [@MODEL_NAME@]? {
   
    @PREDICATE_LINE@
    @SORT_DESCRIPTORS_LINE@
    @FETCH_QUERY_LINE@

    if fetchResults != nil {
        let models = fetchResults!.map { (managedObject) -> @MODEL_NAME@ in
            let model = @MODEL_NAME@.init(managedObject: managedObject);
            return model
        }
        return models;
    }
    return nil;
}
"""

let kFetchQueryLineCodeWithWithSort = """

    let fetchResults = CoreDataManager.shared.fetchRequestForEntity(entityName: "@ENTITY_NAME@", predicate: predicate, sortDescriptors: sortDescriptors) as? [NSManagedObject];
"""

let kFetchQueryLineCodeWithWithoutSort = """

    let fetchResults = CoreDataManager.shared.fetchRequestForEntity(entityName: "@ENTITY_NAME@", predicate: predicate) as? [NSManagedObject];
"""

let kPredicateLineCode = "let predicate =  NSPredicate.init(format: \"@QUERY_PREDICATE_FORMAT@\"@QUERY_PREDICATE_ARGS@ )";

let kSortDescriptorsLineCode = "let sortDescriptors = [NSSortDescriptor(key:\"%@\", ascending:%@)]";



